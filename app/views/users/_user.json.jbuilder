json.extract! user, :id, :name, :receiver, :status, :price, :created_at, :updated_at
json.url user_url(user, format: :json)
