class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :receiver
      t.string :status
      t.integer :price

      t.timestamps
    end
  end
end
